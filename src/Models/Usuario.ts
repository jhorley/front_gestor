export default class Usuario{
    user_id?: number
    nombre: string
    apellidos: string
    email: string
    password: string
    foto?: File
    user_estado: string
    created_at?: Date
    updated_at?: Date

    constructor(){
        this.nombre='';
        this.apellidos='';
        this.email='';
        this.password='';
        this.user_estado='ACTIVO';
    }
}