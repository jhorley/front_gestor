export default class RulesValidate{
     
    public min_5:any;
    public max_10:any;
    public email: any;
    public password: any;
    public requerido: any;
    public min_5_requerido:any;
    public min_1_requerido: any;
    public email_requerido: any;

    constructor(){
        this.requerido          = [(v: any) => !!v || 'Campo es requerido'];
        this.min_5              = [(v: any) => v?.length >= 5 || 'Debe tener mínimo 5 caracteres'];
        this.min_5_requerido    = [(v: any) => !!v || 'Campo es requerido',(v: any)=> v?.length >= 5 || 'Debe tener mínimo 5 caracteres'];
        this.email_requerido    = [(v: any) => !!v || 'Campo es requerido', (v: any)=> /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i.test(v) || 'Email formato no válido'];
        this.email              = [(v: any) => !!v || 'Email es requerido'];
        this.max_10             = [(v: any) => v < 10 || 'Número excede el límite'];
        this.min_1_requerido    = [(v: any) => !!v || 'Campo es requerido', (v: any)=> v > 0 || 'Número no debe ser menor a 1'];
        this.password           = [(v: any) => !!v || 'Contraseña es requerido',  (v: any)=> v.length >= 6 || 'Debe tener mínimo 6 caracteres'];
    }

}