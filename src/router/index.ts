import Vue from 'vue'
import VueRouter from 'vue-router'
import Auth from '../views/Auth/Index.vue'
import Login from '../views/Auth/Login.vue'
import Agenda from '../views/Agenda/Agenda.vue'
import Register from '../views/Auth/Register.vue'
import Usuarios from '../views/Usuarios/Usuarios.vue'
import Dashboard from '../views/Dashboard/Dashboard.vue'
import Productos from '../views/Productos/Productos.vue'
import Categorias from '../views/Categorias/Categorias.vue'
import store from '../store/index'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Inicio',
    component: Dashboard,
    beforeEnter: (to: any , from: any, next: any) => {
      if (isAuthenticated()) next()
      else next({ name: 'Auth' })
    }
  },
  {
    path: '/auth',
    component:Auth,
    redirect: { name: 'Auth' },
    children:[
      {path:'login',component:Login,name:'Auth'},
      {path:'register',component:Register,name:'Auth'},
    ]
  },
  {
    path: '/categorias',
    name: 'Categorias',
    component: Categorias,
    beforeEnter: (to: any , from: any, next: any) => {(isAuthenticated()) ? next() : next({ name: 'Auth' })}
  },
  {
    path: '/productos',
    name: 'Productos',
    component: Productos,
    beforeEnter: (to: any , from: any, next: any) => {(isAuthenticated()) ? next() : next({ name: 'Auth' })}
  },
  {
    path: '/agenda',
    name: 'Agenda',
    component: Agenda,
    beforeEnter: (to: any , from: any, next: any) => {(isAuthenticated()) ? next() : next({ name: 'Auth' })}
  },
  {
    path: '/usuarios',
    name: 'Usuarios',
    component: Usuarios,
    beforeEnter: (to: any , from: any, next: any) => {(isAuthenticated()) ? next() : next({ name: 'Auth' })}
  },
]

const router = new VueRouter({
  routes
})

function isAuthenticated(){
  if(localStorage.getItem('gestor_token')){
      return true
  }else{
    return false;
  }
}

router.beforeResolve((to: any , from: any, next: any) => {
  if (to.name) {
    store.commit('showLoader');
  }
  next()
})

router.afterEach((to, from) => {
    setTimeout(() => {
      store.commit('hideLoader'); 
    }, 1000);
})

export default router
