export default class Tarifa{
    tari_id?: number
    fecha_inicio?: Date
    fecha_fin?: Date
    precio: number
    prod_id: number
    tari_estado: string
    created_at?: Date
    updated_at?: Date

    constructor(){
        this.precio=0;
        this.prod_id=0;
        this.tari_estado='ACTIVO';
    }
}