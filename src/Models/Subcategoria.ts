export default class Subcategoria{
    sub_id?: number
    sub_nombre: string
    sub_codigo: string
    sub_descripcion: string
    sub_estado: string
    created_at?: Date
    updated_at?: Date

    constructor(){
        this.sub_nombre='';
        this.sub_codigo='';
        this.sub_descripcion='';
        this.sub_estado='ACTIVO';
    }
}