import Usuario from '@/Models/Usuario';
import Vue from 'vue'
const axios = require('axios').default;

export default class AuthService{
  
    async registrar(usuario: Usuario){
        const formData = new FormData();
        formData.set('usuario', JSON.stringify(usuario) );
        return axios.post(`${process.env.VUE_APP_API}registrar`,formData)
        .then((resp: any)=>{
            return resp.data      
        }).catch((error: any)=>{
            console.log(error);
            return error;
        })
    }

    async login(user: Usuario){
        return axios.post(`${process.env.VUE_APP_API}login`,user).
        then((resp: any)=>{
            console.log(resp)
            return resp.data      
        }).catch((error: any)=>{
            console.log(error);
            return error;
        })
    }

    enviarCorreo(user_id: any){
        axios.get(`${process.env.VUE_APP_API}sendEmail/${user_id}`).then((resp: any)=>{
            const answer=resp.data;
            Vue.swal.fire(answer.message);
        }).catch((error: any)=>{
            console.log(error);
        });
    }
  }