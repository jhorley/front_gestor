const axios = require('axios').default;
import Producto from '@/Models/Producto'
import Categoria from '@/Models/Categoria'
import Subcategoria from '@/Models/Subcategoria'
import router from '@/router';

export default class ProductoService{

    public config:any;

    constructor(){
        this.config={
            headers: {
                Authorization: localStorage.getItem('gestor_token'),
                'Content-Type': 'multipart/form-data'
            }
        };
    }

    async getProductos(){
        return axios.get(`${process.env.VUE_APP_API}productos`,this.config)
        .then((resp: any)=>{
            if(resp.data.code==200){
                return resp.data;
            }else{
                alert(resp.data.message);
                localStorage.removeItem('gestor_token');
                router.push('auth/login');
            }
        }).catch((error: any)=>{
            return error;
        });
    }

    async saveProducto(producto: Producto, categorias:Categoria[], subcategorias:Subcategoria[]){
        const config={ headers: {Authorization: localStorage.getItem('gestor_token')}};
        return axios.post(`${process.env.VUE_APP_API}productos`,{
            'producto':producto,
            'categorias': categorias,
            'subcategorias':subcategorias
        },config)
        .then((resp: any)=>{
            return resp.data;
        }).catch((error: any)=>{
            return error;
        });
    }

    async editProducto(producto: Producto, categorias:Categoria[], subcategorias:Subcategoria[]){
        const config={ headers: {Authorization: localStorage.getItem('gestor_token')}};
        return axios.put(`${process.env.VUE_APP_API}productos/${producto.prod_id}`,{
            'producto':producto,
            'categorias': categorias,
            'subcategorias':subcategorias
        },config)
        .then((resp: any)=>{
            return resp.data;
        }).catch((error: any)=>{
            return error;
        });
    }

    async deleteProducto(producto: Producto){
        return axios.delete(`${process.env.VUE_APP_API}productos/${producto.prod_id}`,this.config)
        .then((resp: any)=>{
            return resp.data;
        }).catch((error: any)=>{
            return error;
        });
    }

    async getProducto(producto: Producto){
        return axios.get(`${process.env.VUE_APP_API}productos/${producto.prod_id}`,this.config)
        .then((resp: any)=>{
            return resp.data;
        }).catch((error: any)=>{
            return error;
        });
    }

    async subirFotoProducto(foto: File, producto: Producto){
        const formData = new FormData();
        formData.append(`foto`, foto);
        return axios.post(`${process.env.VUE_APP_API}uploadFotoProducto/${producto.prod_id}`,formData,this.config)
        .then((resp: any)=>{
            return resp.data;
        }).catch((error: any)=>{
            return error;
        });
    }

    async saveTarifas(producto: Producto){
        return axios.post(`${process.env.VUE_APP_API}saveTarifas/${producto.prod_id}`,{
            'tarifas':producto.tarifas
        })
        .then((resp: any)=>{
            console.log(resp);
            return resp.data;
        }).catch((error: any)=>{
            return error;
        });
    }
}