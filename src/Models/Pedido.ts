export default class Pedido{
    pedi_id?: number
    fecha_pedido: any
    prod_id?: number
    cantidad: number
    user_id?: number
    pedi_estado: string
    created_at?: Date
    updated_at?: Date

    constructor(){
        this.fecha_pedido='';
        this.cantidad=1;
        this.pedi_estado='ACTIVO';
    }
}