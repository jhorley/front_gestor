import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    drawer:true,
    overlay:true,
  },
  mutations: {
    interruptorMenu (state) {
      state.drawer=!state.drawer
    },
    showLoader(state){
      state.overlay=true;
    },
    hideLoader(state){
      state.overlay=false;
    }
  },
  actions: {
  },
  modules: {
  }
})
