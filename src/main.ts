import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { BTable } from 'bootstrap-vue'
import vuetify from './plugins/vuetify';
import 'bootstrap/dist/css/bootstrap.css';
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';

Vue.config.productionTip = false
Vue.use(VueSweetalert2);

Vue.component('b-table',BTable)
Vue.component('loader-bars', require('./components/Shared/LoaderBars.vue').default);

new Vue({
  router,
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app')
