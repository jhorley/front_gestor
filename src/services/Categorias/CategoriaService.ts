const axios = require('axios').default;
import Categoria from '@/Models/Categoria'
import router from '@/router';

export default class CategoriaService{

    public config:any;

    constructor(){
        this.config={
            headers: {
                Authorization: localStorage.getItem('gestor_token'),
            }
        };
    }

    async getCategorias(){
        return axios.get(`${process.env.VUE_APP_API}categorias`,this.config)
        .then((resp: any)=>{
            if(resp.data.code==200){
                return resp.data;
            }else{
                alert(resp.data.message);
                localStorage.removeItem('gestor_token');
                router.push('auth/login');
            }
        }).catch((error: any)=>{
            return error;
        });
    }

    async saveCategoria(categoria: Categoria){
        return axios.post(`${process.env.VUE_APP_API}categorias`,{
            'categoria':categoria,
        },this.config)
        .then((resp: any)=>{
            return resp.data;
        }).catch((error: any)=>{
            return error;
        });
    }

    async editCategoria(categoria: Categoria){
        return axios.put(`${process.env.VUE_APP_API}categorias/${categoria.cate_id}`,{
            'categoria':categoria
        },this.config)
        .then((resp: any)=>{
            return resp.data;
        }).catch((error: any)=>{
            return error;
        });
    }

    async deleteCategoria(categoria: Categoria){
        return axios.delete(`${process.env.VUE_APP_API}categorias/${categoria.cate_id}`,this.config)
        .then((resp: any)=>{
            return resp.data;
        }).catch((error: any)=>{
            return error;
        });
    }

    async getCategoria(categoria: Categoria){
        return axios.get(`${process.env.VUE_APP_API}categorias/${categoria.cate_id}`,this.config)
        .then((resp: any)=>{
            return resp.data;
        }).catch((error: any)=>{
            return error;
        });
    }

    async saveSubcategorias(categoria: Categoria){
        return axios.post(`${process.env.VUE_APP_API}saveSubcategorias/${categoria.cate_id}`,{
            'categoria':categoria
        }).then((resp: any)=>{
            return resp.data;
        }).catch((error: any)=>{
            return error;
        });

    }
}