export default class Producto{
    prod_id?: number
    nombre_producto: string
    codigo_producto: string
    descripcion_producto: string
    precio_fijo: number
    prod_estado: string
    tarifas?:any
    created_at?: Date
    updated_at?: Date

    constructor(){
        this.nombre_producto='';
        this.codigo_producto='';
        this.descripcion_producto='';
        this.precio_fijo=0;
        this.prod_estado='ACTIVO';
    }
}