const axios = require('axios').default;
import Pedido from '@/Models/Pedido'

export default class ProductoService{

    public config:any;

    constructor(){
        this.config={
            headers: {
                Authorization: localStorage.getItem('gestor_token'),
            }
        };
    }

    async getPedidos(){
        return axios.get(`${process.env.VUE_APP_API}pedidos`,this.config)
        .then((resp: any)=>{
            return resp.data;
        }).catch((error: any)=>{
            return error;
        });
    }

    async savePedido(pedido: Pedido){
        return axios.post(`${process.env.VUE_APP_API}pedidos`,{
            'pedido':pedido
        },this.config)
        .then((resp: any)=>{
            return resp.data;
        }).catch((error: any)=>{
            return error;
        });
    }

    async updatePedido(pedido:Pedido){
        return axios.put(`${process.env.VUE_APP_API}pedidos/${pedido.pedi_id}`,{
            'pedido':pedido
        },this.config)
        .then((resp: any)=>{
            return resp.data;
        }).catch((error: any)=>{
            return error;
        });
    }
}