import Vue from 'vue'
const axios = require('axios').default;
import Usuario from '@/Models/Usuario'
import router from '@/router';

export default class UserService{

    public config:any;

    constructor(){
        this.config={
            headers: {
                Authorization: localStorage.getItem('gestor_token'),
                'Content-Type': 'multipart/form-data'
            }
        };
    }

    async getUsuarios(){
        return axios.get(`${process.env.VUE_APP_API}usuarios`,this.config)
        .then((resp: any)=>{
            if(resp.data.code==200){
                return resp.data;
            }else{
                alert(resp.data.message);
                localStorage.removeItem('gestor_token');
                router.push('auth/login');
            }
        }).catch((error: any)=>{
            return error;
        });
    }

    async saveUsuario(usuario: Usuario){
        const formData = new FormData();
        formData.set('usuario', JSON.stringify(usuario) );
        if(usuario.foto){
            formData.append(`foto`, usuario.foto);
        }
        return axios.post(`${process.env.VUE_APP_API}usuarios`,formData,this.config)
        .then((resp: any)=>{
            return resp.data;
        }).catch((error: any)=>{
            return error;
        });
    }

    async editUsuario(usuario: Usuario){
        const config={ headers: {Authorization: localStorage.getItem('gestor_token')}};
        return axios.put(`${process.env.VUE_APP_API}usuarios/${usuario.user_id}`,{
            'usuario':usuario
        },config)
        .then((resp: any)=>{
            return resp.data;
        }).catch((error: any)=>{
            return error;
        });
    }

    async deleteUsuario(usuario: Usuario){
        return axios.delete(`${process.env.VUE_APP_API}usuarios/${usuario.user_id}`,this.config)
        .then((resp: any)=>{
            return resp.data;
        }).catch((error: any)=>{
            return error;
        });
    }

    async getUsuario(){
        return axios.get(`${process.env.VUE_APP_API}getUsuario`,this.config)
        .then((resp: any)=>{
            if(resp.data.code==200){
                return resp.data;
            }else{
                alert(resp.data.message);
                localStorage.removeItem('gestor_token');
                router.push('auth/login');
            }
        }).catch((error: any)=>{
            return error;
        });
    }

    enviarCorreo(user_id: any){
        axios.get(`${process.env.VUE_APP_API}sendEmail/${user_id}`).then((resp: any)=>{
            const answer=resp.data;
            Vue.swal.fire(answer.message);
        }).catch((error: any)=>{
            console.log(error);
        });
    }
}