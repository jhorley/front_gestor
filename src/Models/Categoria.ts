export default class Categoria{
    cate_id?: number
    nombre_categoria: string
    codigo_categoria: string
    descripcion_categoria: string
    cate_estado: string
    created_at?: Date
    updated_at?: Date

    constructor(){
        this.nombre_categoria='';
        this.codigo_categoria='';
        this.descripcion_categoria='';
        this.cate_estado='ACTIVO';
    }
}